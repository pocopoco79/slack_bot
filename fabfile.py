#-*- coding:utf-8 -*-
import getpass
from fabric import Connection, Config
from invoke import task

@task
def uname(c):
    """Execute uname."""
    result = c.run("uname -n", hide=True)
    print(result.stdout.strip())