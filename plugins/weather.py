import json

import requests
import urllib
from slackbot.bot import listen_to
from slackbot.bot import respond_to

import xml.etree.ElementTree as ET


def get_weather(city_number):
    # 天気のURL設定 city_numberには都市の番号を指定
    url = "http://weather.livedoor.com/forecast/webservice/json/v1?city=%s" % city_number

    # URLを取得
    response = requests.get(url)
    # URL取得結果のチェック(ステータス200番台が返ってくるか)
    response.raise_for_status()
    # データを読み込む
    weather_data = json.loads(response.text)
    return(weather_data)

# 都市コード取得
def get_city_number(area):
    url = "http://weather.livedoor.com/forecast/rss/primary_area.xml"

    req = urllib.request.Request(url)
    # print(req)
    with urllib.request.urlopen(req) as response:
        xml_string = response.read()

    root = ET.fromstring(xml_string)

    num_area = ''
    for value in root.iter('city') :
        if value.attrib.get('title') == area :
            num_area = value.attrib.get('id')
            break
    else :
        num_area = false
    return(num_area)


@respond_to('^(今日|明日|明後日)の(.*)の天気$')
def whether_1(message, day, city_number):
    # 絵文字リストを作る
    dic_weather = {
        '晴れ': 'sunny',
        '曇り': 'cloud',
        '雨': 'rain_cloud',
        '雪': 'snow_cloud',
        '曇時々晴': 'barely_sunny',
    }

    dic_date = {
        '今日': 0,
        '明日': 1,
        '明後日': 2,
    }
    n = get_city_number(city_number)
    w = get_weather(n)
    t = w['forecasts'][dic_date[day]]
    telop = t['telop']

    # 辞書にない天気が来たら絵文字に空文字を設定する
    if telop in dic_weather:
        emoji = ':' + dic_weather[telop] + ':'
    else:
        emoji = ""

    message.reply('%sの%sの天気は%sです%s' % (day, city_number, telop, emoji))

